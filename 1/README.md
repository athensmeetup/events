## Meetup

Title: gitlabathens 1st meetup!

Date: 1st of july 19:00 GR

Duration: 1 hour

Photo: https://about.gitlab.com/images/press/team_and_pets_with_logo_small.jpg

Ask members a question: Can you answer the 5 questions of this form?

1. Can you write us your email? We'll use it only in case we move from meetup.com to another platform.
2. Which is your name at meetup.com?
3. If you have an account at gitlab.com, can you share it so that we can find you there? e.g. @firstletteroffirstnameLastname
4. Which is your reason for attending this event? Do you have a talk you want to give, a topic you want to hear about, or constructive criticism?
5. How did you learn about this group or this event?

Attendees will be required to provide the following information: First and last name, current role, organization, main reason for attending

### Description

This online workshop is to get to know each other and be more familiar with gitlab by doing a small hands-on challenge in two random groups, so you can create an account at gitlab.com if you don't have one yet.

You can open your camera but note that the event will be recorded.

Agenda for about 60 minutes:

1. A quick walkthrough at zoom and gitlab
2. Introducing the Athens Gitlab community, what to expect and what you can do
3. We'll split into two groups for 30 minutes
4. Come back and share your findings

Useful links:

1. Once you RSVP at https://www.meetup.com/gitlabathens you'll find the link to zoom
2. our values - https://gitlab.com/athensmeetup/events
3. detailed agenda - https://gitlab.com/athensmeetup/events/-/tree/master/1
4. event creation - https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/-/issues/425

Share our group at https://meetup.com/gitlabathens and our videos at https://gitlabathens.com with your colleagues, friends and on social media as #gitlabathens

Emails to servicedesk@gitlabathens.com will open issues at https://gitlab.com/athensmeetup/events

Emails to nick@gitlabathens.com will be received by Nick Maris


## Details of the agenda

### Before

Optionally, before you join, you can save some time by:

1. downloading and installing the zoom client at https://zoom.us/download
2. create an account at gitlab.com if you don't have one yet

### At

To get the link to zoom, you need to RSVP at https://www.meetup.com/gitlabathens

Feel free to open your camera to get to know each other but note that the event will be recorded.

- [x] When joining zoom, write on chat your name at meetup.com, your hashtag at gitlab.com and whether you are in Athens or not.

What about the day and time of the event? 
  
1. John: Focus more on having an audience that is active, participating and doing things rather than on the number of RSVPs or participants at zoom.
2. Nick: That being said, it's better to have a few people available in a few weeks than sending time on promoting an event in 4 months. We opened an issue to vote for the date of the next event, so please vote by putting an emoji. Adding options is fine as long as they have an emoji too :smile: https://gitlab.com/athensmeetup/events/-/issues/9

Would you like to present? If not, then what topic would you like to see?

1. Stavros: A percentage of each event time (or a separate zoom room) could be dedicated to newcomers who are not familiar with gitlab
2. Evaggelos: Learn how others use gitlab, also in Greek if there is noone who doesn't speak Greek.
3. George: We have many options for topics to present but I suggest also people to create an account at gitlab.com and open issues at this repo for any question/suggestion they have. Gitlab pages with various static site generators can demonstrate gitlab handbook along with code review. DAG has cut CI time to half. We are working now on runners setup.

### After

After you join, fill this form which is to confirm the number of attendees and get feedback for improvements. Would you **share** this meetup to colleagues, friends, and on social media?
