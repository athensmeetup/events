## Our values explained

Our actions are based on what we value so that we have a culture that helps us and really represents us.

The values are written in a way that we can take action whereas this file elaborates on why it helps us to have each value.

### Code of Conduct

To ensure the safety and comfort of all participants.

### Low level of shame

Failures are allowed. Nobody knows everything and we are here to learn from each other and have fun, so no judgement please.

Building trust is not only about credibility and reliability but also about proximity. Most people avoid tough conversations like giving honest and productive feedback, we shouldn't. We should embrace vulnerability and the openness to accept it. However, a cadence of at least one event per quarter is also needed to maintain proximity. If we do not have a sense of caring towards someone we lead and/or we don't feel connected to that person, we have two options – develop the caring and connection or find a leader who's a better fit.

### Transparency

That's the hardest to explain if you are not already practicing it.

"Digital communication is much more challenging than physical communication, because it lacks the ability to really transfer tone well," explains [Elaine Swann](https://www.wired.com/story/slack-office-tips/amp), etiquette expert and founder of the Swann School of Protocol. "The inflections in our voice get lost, and we're not able to hear or emphasize body language that sometimes speaks louder than words."

Frequently the important message that is communicated through voice/body is the intention behind our words. When we offer feedback and receive feedback, human habit creates cognitive distortion by defaulting to the most negative aspects of that feedback. In the world of psychology, this is called mental filtering, and it's something that all humans have a tendency to do. Though in software this affliction can be more common, as working in software goes hand-in-hand with valuing yourself based on how intelligent others think you are. Gitlab [assumes positive intent](https://about.gitlab.com/handbook/values/#assume-positive-intent) and advocates [communicating intent](https://about.gitlab.com/blog/2020/06/08/better-code-reviews).

Sometimes the intention is the context, the reasons behind a request or an initiative. Without sharing such intention then you'll either have people contributing while being completely lost on why this is taking place or you'll miss the opportunity of interracting with people who would be interested in the context if they knew about it. In both cases, people won't build on each other's context.

So, as regards being open about as many things as possible, to understand how many things are enough, make sure you communicate intent too. Sometimes writing that down might be more than the original thing you wanted to write down but it is worth it. As Amir Salihefendic, CEO of Doist has said "I am not in your mind, so you have to speak up".

### Bias for Action

That's easy to say but hard to do if you are not used to so following the previous values will help you a lot on that.