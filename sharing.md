We document how we did the announcement of the first event as a reference to organizers of future events and as a placeholder for improvements.

## Meetup

meetup message to: all members subscribed to organizer messages
title: meetup overview and first online event of gitlabathens
body: For an overview of our meetup group, check this youtube video: https://www.youtube.com/watch?v=0F9GMMXYf4U

### cross-posting to Atlassian Athens meetup

meetup message to: all members subscribed to organizer messages
title: meetup overview and first online event of gitlabathens
body: overview and first online event of gitlabathens
For an overview of the new gitlab athens meetup, check [this](https://www.youtube.com/watch?v=0F9GMMXYf4U) youtube video.
For an overview of the new #gitlabathens meetup, check this youtube video.

Ioannis Papikas is also busy with the Product School Athens meetup, so if you can't wait for the next Atlassian event, feel free to organize one. Due to covid, an in-person event might be too early so all you need for an online event is a speaker 

## Linkedin

Upload video from local file and upload thumbnail. Youtube URL is optimized for youtube mobile, not for linkedin desktop.
Post: Press the Attend button to keep our zoom link for the 1st of july 19:00 GR https://www.meetup.com/gitlabathens/events/271315358 #gitlabathens #gitlab

## Twitter

twitter: First #gitlabathens online event on the 1st of july 19:00 GR https://youtu.be/0F9GMMXYf4U #gitlabathens #gitlab

twitter: Press the Attend button to keep our zoom link for the 1st of july 19:00 GR https://www.meetup.com/gitlabathens/events/271315358/ #gitlabathens #gitlab #Meetup

## Requests to other communities to spread the news

https://twitter.com/open_conf/status/1276437699319267328

https://ellak.gr/events/first-gitlab-athens-online-event-on-the-1st-of-july-1900/