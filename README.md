We're a community of people interested in the GitLab platform and open source project.

We prefer a networking session with many interractions, rather than a webinar with many silent attendees.

## What you can do at Gitlab Athens

To join our next event, if it's an online event press the Attend button at meetup to get the link to zoom, otherwise press the RSVP to reserve a seat. The comments section at the meetup event is a nice place to share links with the people who joined.

To state a topic you would like to watch and gather interest, you can [open](https://gitlab.com/athensmeetup/events/-/issues/new) an issue.

To present one, choose the template topic-to-present.

To become a host, a sponsor, help us spread the word or [ask any question](https://about.gitlab.com/blog/2020/03/05/get-involved-with-gitlab-meetups), please [contact us](mailto:nick@gitlabathens.com).

## Where you can find us

At https://meetup.com/gitlabathens

If you want to edit/create any file in this repository, you are invited to request access.

## Our values

Our actions are based on what we value so that we have a culture that helps us and really represent us.

1. [Code of Conduct](https://about.gitlab.com/company/culture/contribute/coc): Harassment is not tolerated and we practice saying "Yes and" to each other which is a theatre improvisation technique to build on each other's ideas. 
2. [Low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame): We value doing small things without a lot of predicting, and without a lot of explaining in order to get feedback over hiding something in fear of receiving judgement.
3. [Bias for Action](https://about.gitlab.com/handbook/values/#bias-for-action): We value doing something now over waiting for example for 3 more events.
4. Respect people's time and attention: We respect people's time on the event through an agenda like Intro-Talks-Questions and an agreed duration. Also, we respect people's attention and we avoid spamming people with announcements that are more noisy than informative.

