```markdown
<!-- 
When volunteering to present a topic, opening an issue is optional and leaving sections below empty is fine too.
However, if you do open an issue, you can ask the organizers to assist you in your preparation, set expectations for anyone looking for a detailed agenda and use it to share links in the end of your presentation if you want.
-->

# Topic

## 💡 Description

<!--
How did you learn more about the topic? How can others benefit from your story? Which key points would you want to discuss/highlight? Sometimes just sharing a story is enough to trigger a very nice discussion, you might be surprised that the topic is not relevant only to you. As a tip, when you're giving a talk or planning an event make sure you know the answer to this question: What do you want people to leave thinking / feeling / doing / saying about it? To start a conversation you can ask e.g. How were you impacted by the situation?
-->

## ⏰ Time

How much time do you need? This is to help us come up with an agenda of topics.

## ✍ Relevant URLs

- [Website]()
- [Demo repo]()

## 💻 Resources

Can you prepare slides, live demo, a short story or a blog post? Pick all that apply.

- [ ] Presentation/Slides
- [ ] Live demo
- [ ] Blog post
- [ ] Story 
- [ ] Others: Please specify

```